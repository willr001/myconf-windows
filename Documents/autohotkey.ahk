#UseHook        ; force keyboard hook
#SingleInstance ; don't allow multiple instances
#NoEnv          ; don't try to expand unknown vars as env vars
#Warn           ; Enable warnings to assist with detecting common errors.
;#NoTrayIcon     ; don't show tray
SendMode Input  ; Recommended for new scripts due to its speed and reliability.


; ==============================================================================
; INITIALIZE
; ==============================================================================

; retrieve environment variables needed by script
; -----------------------------------------------

VarSetCapacity(env_home, 500)
EnvGet, env_home, home

VarSetCapacity(env_pf86, 500)
EnvGet, env_pf86, programfiles(x86)

VarSetCapacity(env_pf, 500)
EnvGet, env_pf, programfiles

conemu = "%env_pf%\ConEmu\ConEmu64.exe" -dir "%env_home%" -run


; ==============================================================================
; CAPTROLSCAPE
; ==============================================================================


; Three way capslock key (assuming registry scancode remapped to RControl)
;     <+Caps       - SetCapsOn
;     Caps(short) - CapsOn ? CapsOff : Esc
;     Caps(long)  - >^

~+RControl:: SetCapsLockState AlwaysOn
~RControl:: KeyWait RControl
RControl UP::
    Send {RControl UP}
    if ((A_TimeSincePriorHotkey < 300) && (A_PriorKey == "RControl")) {
        if (GetKeyState("CapsLock","T")) {
            SetCapsLockState AlwaysOff
        } else {
            Send {Esc}
        }
    }
    return


; ==============================================================================
; WINDOWS KEY
; ==============================================================================


; Make Win key only open start on taps
; ------------------------------------
; KeyWait makes the "time since" bs work properly in the "UP" events
; On Up release key and if short press and no other keys pressed, send win key.

~LWin:: KeyWait LWin
LWin Up::
    Send {LWin UP}
    if ((A_TimeSincePriorHotkey < 300) && (A_PriorKey == "LWin"))
        Send {vk5B}
    return

~RWin:: KeyWait RWin
RWin Up::
    Send {RWin UP}
    if ((A_TimeSincePriorHotkey < 300) && (A_PriorKey == "RWin"))
        Send {vk5B}
    return


; Kill microsoft narrator
; -----------------------

#u:: Send ^+!#u


; Enter - Apps key for shitty keyboards
; -------------------------------------

#Enter:: Send {AppsKey}


; Space - Launchy (bind in Launchy)
; ---------------------------------

#Space:: Send ^+{Space}


; R - Run...
; ----------

#r:: Run %A_ScriptDir%\autohotkey_support\Run.lnk


; E - Explore
; -----------
;   #  - FileExplorer
;   #+ - FreeCommander
;   #! - FarManager

#e::  Run explorer
#+e:: Run freecommander
#!e:: Run %conemu% {FAR::far}


; D - Desktop/DOS
; ---------------
;   #  - Desktop
;   #^ - Dos prompt
;   #! - Debian prompt
;   adding shift (+) makes prompts admin

#d::   Run %A_ScriptDir%\autohotkey_support\Desktop.lnk
#^d::  Run %conemu% {CMD::cmd}
#^+d:: Run %conemu% {CMD::adminCmd}
#!d::  Run %conemu% {BASH::bash}
#!+d:: Run %conemu% {BASH::adminBash}


; F - Find in...
; --------------
;   #     - Windows
;   #+    - Everything (bind in program, not done here)
;   #[^!] - Internet

#f::Run %A_ScriptDir%\autohotkey_support\search.lnk
#^f::GoogleSearch("")
#!f::GoogleSearch("--incognito")

GoogleSearch(searchswitch)
{
    VarSetCapacity(env_pf86_, 500)
    EnvGet, env_pf86_, programfiles(x86)
    InputBox, searchString, "Enter web search string"
    Run "%env_pf86_%\Google\Chrome\Application\chrome.exe" %searchswitch% -d "? %SearchString%"
}


; Break - System
; --------------
;   #   - SystemProperties
;   #+  - NetworkConnections
;   #!  - ControlPanel
;   #!+ - ComputerManagement

#Break::   Run %A_ScriptDir%\autohotkey_support\System.lnk
#+Break::  Run %A_ScriptDir%\autohotkey_support\NetworkConnections.lnk
#!Break::  Run %A_ScriptDir%\autohotkey_support\ControlPanel.lnk
#!+Break:: Run %A_ScriptDir%\autohotkey_support\ComputerManagement.lnk


; ==============================================================================
; TEN KEY
; ==============================================================================


; Numpad Invert - Handled by programmer Dvorak
; -------------------------------------------
; swaps 123 with 789 to invert the numbers like an ATM/phone

 
; +NumpadX - Shifted numpad characters, handled by programmer Dvorak
; ---------------------------------------
; /*  :: ()
; -+  :: $,
; 1-6 :: a-f
; 78  :: =x
; 9.  :: :;
+NumpadDiv::   Send {(}
+NumpadMult::  Send {)}
+NumpadAdd::   Send {,}
+NumpadSub::   Send {$}
+NumpadDot::   Send {;}
+Numpad0::     Send {\}
+Numpad1::     Send {a} 
+Numpad2::     Send {b}
+Numpad3::     Send {c}
+Numpad4::     Send {d}
+Numpad5::     Send {e}
+Numpad6::     Send {f}
+Numpad7::     Send {x}
+Numpad8::     Send {=}
+Numpad9::     Send {:}

; #NumpadX - Extended Media Keys
; ------------------------------

#NumpadDiv::   Send {Browser_Search}
#NumpadMult::  Send {Browser_Favorites}

#NumpadAdd::   Send {Volume_Up}
#NumpadSub::   Send {Volume_Down}
#NumpadEnter:: Send {Volume_Mute}
#NumpadDot::   Send {Volume_Mute}

#Numpad0::     Send {Media_Play_Pause}

; alternate for inverted 10 key
#Numpad7::     Send {Media_Prev}
#Numpad8::     Send {Media_Play_Pause}
#Numpad9::     Send {Media_Next}

/*
; alternate for standard 10 key 
#Numpad1::     Send {Media_Prev}
#Numpad2::     Send {Media_Play_Pause}
#Numpad3::     Send {Media_Next}
*/

#Numpad4::     Send {Launch_Mail}
#Numpad5::     Run calc
#Numpad6::     Send {Launch_Media}

; alternate for inverted 10 key
#Numpad1::     Send {Browser_Back}
#Numpad2::    Send {Browser_Refresh}
#!Numpad2::    Send {Browser_Stop}
#Numpad3::     Send {Browser_Forward}

/*
; alternate for standard 10 key
#Numpad7::     Send {Browser_Back}
#Numpad8::    Send {Browser_Refresh}
#!Numpad8::    Send {Browser_Stop}
#Numpad9::     Send {Browser_Forward}
*/

; ^NumpadX - Numlock Off without Numlock Off
; ------------------------------------------

^Numpad1::   Send {vk24} ;home
^Numpad2::   Send {vk26} ;up
^Numpad3::   Send {vk21} ;pgup
^Numpad4::   Send {vk25} ;left
^Numpad6::   Send {vk27} ;right
^Numpad7::   Send {vk23} ;end
^Numpad8::   Send {vk28} ;down
^Numpad9::   Send {vk22} ;pgdn
^Numpad0::   Send {vk2d} ;insert
^NumpadDot:: Send {vk2e} ;delete


; Trying to fix the lack of numlock in programmer dvorak in windows
; -----------------------------------------------------------------

Numpad1::   FixTk("Numpad1",   "vk24") ;home
Numpad2::   FixTk("Numpad2",   "vk26") ;up
Numpad3::   FixTk("Numpad3",   "vk21") ;pgup
Numpad4::   FixTk("Numpad4",   "vk25") ;left
Numpad6::   FixTk("Numpad6",   "vk27") ;right
Numpad7::   FixTk("Numpad7",   "vk23") ;end
Numpad8::   FixTk("Numpad8",   "vk28") ;down
Numpad9::   FixTk("Numpad9",   "vk22") ;pgdn
Numpad0::   FixTk("Numpad0",   "vk2d") ;insert
NumpadDot:: FixTk("NumpadDot", "vk2e") ;delete

FixTk(keypress, keycode)
{
    If GetKeyState("NumLock","T") {
        Send {%keypress%}
    } else {
        Send {%keycode%}
    }
}


; ==============================================================================
; VIM CURSOR KEYS
; ==============================================================================


; h = left/home
; -------------

#h::Send {vk25}
#!h::Send {vk24}


; j = down/pgDown
; ---------------

#j::Send {vk28}
#!j::Send {vk22}


; l = right/end
; -------------

#l::Send {vk27}
#!l::Send {vk23}


; k = up/pgUp
; -----------

#k::Send {vk26}
#!k::Send {vk21}

