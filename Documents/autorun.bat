START "C:\Program Files\AutoHotkey\AutoHotkey.exe"

:: removes dropbox icon from nav pane
REG ADD "HKEY_CLASSES_ROOT\CLSID\{E31EA727-12ED-4702-820C-4B6445F28E1A}"^
 /v "System.IsPinnedToNamespaceTree"^
 /t REG_DWORD^
 /d 00000000^
 /f