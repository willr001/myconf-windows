# ========================================================================
#
# Copyright 2011 Joshua Taylor (Josh.Taylor@ManagedUx.com)
#
# This script is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, Or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this script.  If not, see http://www.gnu.org/licenses/.
#
# ========================================================================
#
If($True -eq ([Security.Principal.WindowsPrincipal] [Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator"))
{
	(Get-Host).UI.RawUI.BackgroundColor = "darkred"
	(Get-Host).UI.RawUI.ForegroundColor = "white"
	
	# Capture the host buffer so it can be repainted 
	$bufferWidth = $host.ui.rawui.BufferSize.Width 
	$bufferHeight = $host.ui.rawui.CursorPosition.Y 
	$rec = new-object System.Management.Automation.Host.Rectangle 0,0,($bufferWidth - 1),$bufferHeight 
	$buffer = $host.ui.rawui.GetBufferContents($rec)

	# Clear the background to the new colors
	Clear-Host

	# Repaint the captured buffer back to the screen. 
	$YOffset = [console]::WindowTop 
	for($i = 0; $i -lt $bufferHeight; $i++) 
	{ 
		$bufferLine = New-Object System.Text.StringBuilder
  		for($j = 0; $j -lt $bufferWidth; $j++) 
  		{ 
  			$char = $buffer[$i,$j]
			$null = $bufferLine.Append($char.Character)	
  		} 
		[console]::setcursorposition(0,$YOffset+$i)
		Write-Host $bufferLine.ToString() -NoNewline 
	}	 
	
	# If admin set the trailing prompt character to a hash
	$GLOBAL:PromptTrail = "#"
}
Else
{
	# If nonadmin all we need to do is the trailing prompt character to the arrow
	$GLOBAL:PromptTrail = ">"
}

# Overwrite the prompt function to use our selected trailing character
Function Prompt
{
	Write-Output $("PS " + $(Get-Location) + $GLOBAL:PromptTrail + " ") -NoNewline
}

# If admin write out a warning
If($True -eq ([Security.Principal.WindowsPrincipal] [Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator"))
{
	Write-Output ""
	Write-Output "CAUTION: PowerShell is running with administrator credentials!"
	Write-Output ""
	Write-Output ""
}