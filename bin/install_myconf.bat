SETLOCAL EnableExtensions

:: disables hibernate to save RAM_SIZE on C:
POWERCFG /h off
:: remove all pinned items from taskbar
DEL /F /S /Q /A "%AppData%\Microsoft\Internet Explorer\Quick Launch\User Pinned\TaskBar\*"
REG DELETE HKCU\Software\Microsoft\Windows\CurrentVersion\Explorer\Taskband /F
:: removes the onedrive icon from the nav pane
REG ADD "HKEY_CLASSES_ROOT\CLSID\{018D5C66-4533-4307-9B53-224DE2ED1FE6}"^
 /v "System.IsPinnedToNameSpaceTree" /t REG_DWORD /d 00000000 /f
:: removes dropbox icon from nav pane
REG ADD "HKEY_CLASSES_ROOT\CLSID\{E31EA727-12ED-4702-820C-4B6445F28E1A}"^
 /v "System.IsPinnedToNamespaceTree" /t REG_DWORD /d 00000000 /f
choco upgrade git
call refreshenv
:: installs myconf custom files
GIT clone --bare https://bitbucket.org/willr001/myconf-windows^
 %userprofile%\.myconf
MKDIR %userprofile%\bin
ECHO git --git-dir="%%userprofile%%\.myconf" --work-tree="%%userprofile%%" %%*^
 >%userprofile%\bin\myconf.cmd
CALL refreshenv
CALL "%userprofile%\bin\myconf" config --local status.showUntrackedFiles no
CALL "%userprofile%\bin\myconf" checkout

:: restart to finish installing
ECHO.RESTARTING in 15 seconds, PRESS ANY KEY TO ABORT
SHUTDOWN -r -f -t 15 -c "Reboot required for dotfiles installer"
PAUSE>nul
SHUTDOWN -a
ECHO.RESTART ABORTED

EXIT /B