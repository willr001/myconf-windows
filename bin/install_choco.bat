﻿SETLOCAL EnableExtensions
IF "%HOME%" == "" SETX HOME %usrn%
SET "USRBIN=%USERPROFILE%\bin"
mkdir "%USRBIN%"
SET "SHIMGEN=c:\programdata\chocolatey\tools\shimgen.exe"

:: disables hibernate to save RAM_SIZE on C:
POWERCFG /h off

:: remove all pinned items from taskbar
DEL /F /S /Q /A "%AppData%\Microsoft\Internet Explorer\Quick Launch\User Pinned\TaskBar\*"
REG DELETE HKCU\Software\Microsoft\Windows\CurrentVersion\Explorer\Taskband /F

:: removes the onedrive icon from the nav pane
REG ADD "HKEY_CLASSES_ROOT\CLSID\{018D5C66-4533-4307-9B53-224DE2ED1FE6}"^
 /v "System.IsPinnedToNameSpaceTree" /t REG_DWORD /d 00000000 /f
choco upgrade git
call refreshenv

:: install chocolatey
POWERSHELL -NoProfile -ExecutionPolicy Bypass^
 -Command "iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))" && SET "PATH=%PATH%;%ALLUSERSPROFILE%\chocolatey\bin"
CALL %programdata\chocolatey\redirects\refreshenv.cmd
CHOCO feature enable -n=allowGlobalConfirmation
CHOCO feature enable -n=allowEmptyChecksums

:: installs myconf custom files
GIT clone --bare https://bitbucket.org/willr001/myconf-windows^
 %userprofile%\.myconf
MKDIR %userprofile%\bin
ECHO git --git-dir="%%userprofile%%\.myconf" --work-tree="%%userprofile%%" %%*^
 >%userprofile%\bin\myconf.cmd
CALL refreshenv
CALL "%userprofile%\bin\myconf" config --local status.showUntrackedFiles no
CALL "%userprofile%\bin\myconf" checkout

:: installs a retarded quantity of programs
CHOCO upgrade chocolatey android-sdk astyle autohotkey ccleaner GoogleChrome^
 deluge doxygen git hexedit kdiff3 vlc PowerShell putty thunderbird^
 libreoffice virtualbox dropbox ConEmu Wget CutePDF googledrive mingw^
 tortoisehg f.lux Everything PDFXChangeViewer launchy rufus inkscape^
 VisualStudio2015Community xmlnotepad lua activeperl activetcl audacity cmake^
 ctags curl firefox gimp infrarecorder jdk8 virtualdub^
 --timeout=9999^
 -y^
 --allow-empty-checksums^
 --ignore-checksums
 
CHOCO upgrade
 
CALL refreshenv
 
CHOCO upgrade clink
%SHIMGEN% -o "%USRBIN%\clink.exe"^
 -p "%programfiles(x86)%\clink\clink.exe"

CHOCO upgrade cppcheck
%SHIMGEN% -o "%USRBIN%\cppcheck.exe"^
 -p "%programfiles%\CppCheck\CppCheck.exe"

CHOCO upgrade doxygen
%SHIMGEN% -o "%USRBIN%\doxygen.exe"^
 -p "%programfiles%\doxygen\doxygen.exe"

CHOCO upgrade far
%SHIMGEN% -o "%userbin%\far.exe"^
 -p "%programfiles%\Far Manager\far.exe"

CHOCO upgrade freecommander
%SHIMGEN% -o "%USRBIN%\fc.exe"^
 -p "%programfiles(x86)%\FreeCommander XE\freecommander.exe" --gui

CHOCO upgrade irfanview irfanviewplugins
%SHIMGEN% -o "%USRBIN%\iview.exe"^
 -p "%programfiles%\IrfanView\i_view64.exe" --gui

CHOCO upgrade notepadplusplus
%SHIMGEN% -o "%USRBIN%\npp.exe"^
 -p "%programfiles%\notepad++\notepad++.exe"

CHOCO upgrade PaintDotNet
%SHIMGEN% -o "%USRBIN%\pdn.exe"^
 -p "%programfiles%\Paint.Net\PaintDotNet.exe" --gui

CHOCO upgrade picasa
%SHIMGEN% -o "%USRBIN%\Picasa.exe"^
 -p "%programfiles(x86)%\Picasa3\Picasa3.exe" --gui

CHOCO upgrade plex-HOME-theatre

%SHIMGEN% -o "%USRBIN%\pspdash.exe"^
 -p "%binlib%\ProcessDashboard\ProcessDashboard.exe" --gui

::CHOCO upgrade programmer-dvorak
:: set programmer dvorak as alternate key layout
REG ADD "HKCU\Keyboard Layout\Preload" /v "1" /t REG_SZ /d 00000409 /f
REG ADD "HKCU\Keyboard Layout\Substitutes"^
 /v "00000409" /t REG_SZ /d 19360409 /f
:: set numlock on with windows boot
REG ADD "HKCU\.DEFAULT\Control Panel\Keyboard"^
 /v "InitialKeyboardIndicators" /t REG_SZ /d 2 /f
:: disable most Win+X shortcuts
REG ADD "HKCU\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\Explorer"^
 /v "NoWinKeys" /t REG_DWORD /d 00000001 /f
:: remap capslock to RControl
REG ADD "HKLM\SYSTEM\CurrentControlSet\Control\Keyboard Layout"^
 /v "Scancode Map" /t REG_BINARY /d 0000000000000000020000001de03a0000000000 /f

CHOCO upgrade python python2
SETX PYTHONHOME c:\tools\python
MKLINK /J c:\tools\python c:\tools\python35

CHOCO upgrade racket
SETX RACKETHOME "%programfiles%\racket"
CALL refreshenv
GIT clone "https://github.com/profan/swank-racket" "%rackethome%\swank-racket"
%SHIMGEN% -o "%USRBIN%\Racket.exe" -p "%rackethome%\Racket.exe"
ECHO start /min cmd /c "%RACKETHOME%\racket" "%RACKETHOME%\swank-racket\server.rkt"^
 >"%USRBIN%\swank-racket.bat"

CHOCO upgrade reshack
%SHIMGEN% -o "%USRBIN%\reshack.exe"^
 -p "%programfiles(x86)%\Resource Hacker\ResourceHacker.exe" --gui

CHOCO upgrade ruby
SET "olddir=%cd%"
CD "%~dp0"
WGET https://curl.haxx.se/ca/cacert.pem
MKDIR "%USERPROFILE%\.gem"
MOVE cacert.pem "%USERPROFILE%\.gem"
SETX SSL_CERT_FILE "%USERPROFILE%\.gem\cacert.pem"
CD "%olddir"
CALL refreshenv
CALL gem update --system
CALL gem install rake
CALL gem install ceedling
CALL gem update

%SHIMGEN% -o "%USRBIN%\SourceMonitor.exe"^
 -p "%binlib%\SourceMonitor\SourceMonitor.exe"

CHOCO upgrade vim-x64
%SHIMGEN% -o "%USRBIN%\vim.exe" -p "%programfiles%\vim\vim80\vim.exe"
%SHIMGEN% -o "%USRBIN%\gvim.exe" -p "%programfiles%\vim\vim80\gvim.exe"
ECHO vim --remote-silent>"%USRBIN%\vi.bat"
ECHO gvim --remote-silent>"%USRBIN%\gvi.bat"
MKDIR "%HOME%\vimfiles\bundle"
RMDIR /s /q "%HOME%\vimfiles\bundle\vundle.vim"
CALL git clone https://github.com/vundlevim/vundle.vim^
 "%HOME%\vimfiles\bundle\vundle.vim"
VIM +PluginInstall +qall!
:contin
CHOCO upgrade windirstat
%SHIMGEN% -o "%USRBIN%\windirstat.exe"^
 -p "%programfiles(x86)%\windirstat\windirstat.exe" --gui

CHOCO upgrade XmlNotepad
%SHIMGEN% -o "%USRBIN%\XmlNotepad.exe"^
 -p "%programfiles(x86)%\XML Notepad 2007\XmlNotepad.exe" --gui

:: installs fonts
SET "olddir=%cd%"
CD "%~dp0"
WGET https://github.com/madmalik/mononoki/raw/master/export/mononoki.zip
WGET https://github.com/nicolalamacchia/powerline-consolas/raw/master/consola.ttf
CD "%olddir%"
ECHO EXTRACTING FONTS....
7Z e -o"%~dp0" "%~dp0mononoki.zip"
ECHO PARSING FONTS....
FOR /F %%f in ('dir /b "%~dp0*.*tf"') DO (CALL :font "%~dp0%%f")
ECHO CLEANING UP FONTS....
DEL "%~dp0mononoki*"
DEL "%~dp0consola*"

:: replaces c:\windows\notepad
CHOCO install notepadreplacer --force

:: restart to finish installing
ECHO.RESTARTING in 15 seconds, PRESS ANY KEY TO ABORT
SHUTDOWN -r -f -t 15 -c "Reboot required for dotfiles installer"
PAUSE>nul
SHUTDOWN -a
ECHO.RESTART ABORTED
EXIT /B

::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

:: sub to install a font
:font
IF "%~x1"==".otf" SET FTYPE=(OpenType)
IF "%~x1"==".ttf" SET FTYPE=(TrueType)
COPY /Y "%~f1" "%SystemRoot%\Fonts\"
REG add "HKLM\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Fonts"^
 /v "%~n1 %FTYPE%" /t REG_SZ /d "%~nx1" /f
EXIT /B